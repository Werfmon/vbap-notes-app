package cz.osu.r23179.vbapnotesapi.service.impl;

import cz.osu.r23179.vbapnotesapi.exception.ResourceNotFoundException;
import cz.osu.r23179.vbapnotesapi.model.entity.Group;
import cz.osu.r23179.vbapnotesapi.model.entity.Note;
import cz.osu.r23179.vbapnotesapi.repository.GroupRepository;
import cz.osu.r23179.vbapnotesapi.repository.NoteRepository;
import cz.osu.r23179.vbapnotesapi.service.GroupService;
import cz.osu.r23179.vbapnotesapi.service.NoteService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class GroupServiceImpl implements GroupService {

    private final GroupRepository groupRepository;
    private final NoteService noteService;


    @Override
    public Group createGroup(Group group) {
        return groupRepository.save(group);
    }

    @Override
    public Group getById(UUID id) {
        return groupRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(STR."Cannot find group with id: \{id}"));
    }

    @Override
    public List<Group> getAllGroups() {
        return groupRepository.findAll();
    }

    @Override
    public Group updateGroup(Group current, Group group) {
        current.setName(group.getName());
        return groupRepository.save(current);
    }

    @Override
    public void deleteGroup(Group group) {
        List<Note> notes = group.getNotes()
                .stream()
                .peek(note -> note.setGroup(null))
                .toList();
        noteService.saveAllNotes(notes);

        groupRepository.delete(group);
    }
}
