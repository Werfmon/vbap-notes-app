package cz.osu.r23179.vbapnotesapi.service;

import cz.osu.r23179.vbapnotesapi.model.entity.User;

import java.util.List;
import java.util.UUID;

public interface UserService {
    User createUser(User user);

    User getById(UUID id);

    List<User> getAllUsers();

    User updateUser(User current, User user);

    User getCurrentUser();

    User getUserByEmail(String email);

    String authenticateUser(String email, String password);

    void deleteUser(User user);
}
