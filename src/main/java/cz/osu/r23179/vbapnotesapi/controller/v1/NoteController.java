package cz.osu.r23179.vbapnotesapi.controller.v1;

import cz.osu.r23179.vbapnotesapi.mapper.NoteMapper;
import cz.osu.r23179.vbapnotesapi.model.dto.note.NoteGetDTO;
import cz.osu.r23179.vbapnotesapi.model.dto.note.NotePostDTO;
import cz.osu.r23179.vbapnotesapi.model.dto.note.NotePutDTO;
import cz.osu.r23179.vbapnotesapi.model.entity.Note;
import cz.osu.r23179.vbapnotesapi.service.GroupService;
import cz.osu.r23179.vbapnotesapi.service.NoteService;
import cz.osu.r23179.vbapnotesapi.service.TagService;
import cz.osu.r23179.vbapnotesapi.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/v1/notes")
@RequiredArgsConstructor
@Tag(name = "Note controller", description = "Controller for operations with notes")
public class NoteController {
    private final NoteService noteService;
    private final NoteMapper noteMapper;
    private final UserService userService;
    private final GroupService groupService;

    @Operation(summary = "EP for create note", responses = {
            @ApiResponse(
                    description = "Validation error",
                    content = @Content(mediaType = "application/json"),
                    responseCode = "422"
            ),
            @ApiResponse(
                    description = "Successful request",
                    responseCode = "200",
                    content = @Content(mediaType = "application/json"),
                    useReturnTypeSchema = true
            )
    })
    @PostMapping
    public NoteGetDTO createNote(@Valid @RequestBody NotePostDTO notePostDTO) {
        Note note = noteMapper.toEntity(notePostDTO);

        if (notePostDTO.getGroupId() != null) {
            note.setGroup(groupService.getById(notePostDTO.getGroupId()));
        }
        note.setUser(userService.getCurrentUser());

        return noteMapper.toDTO(noteService.saveNote(note));
    }
    @Operation(summary = "EP for obtain note by id", responses = {
            @ApiResponse(
                    description = "Note not found",
                    responseCode = "404",
                    content = @Content(mediaType = "application/json"),
                    useReturnTypeSchema = true
            ),
            @ApiResponse(
                    description = "Successful request",
                    responseCode = "200",
                    content = @Content(mediaType = "application/json"),
                    useReturnTypeSchema = true
            )
    })
    @GetMapping("/{id}")
    public NoteGetDTO getNoteById(@PathVariable UUID id) {
        return noteMapper.toDTO(noteService.getById(id));
    }

    @Operation(summary = "EP for obtain all notes")
    @GetMapping
    public List<NoteGetDTO> getNotes() {
        return noteMapper.toDTO(noteService.getAllNotes());
    }

    @Operation(summary = "EP for update note by id", responses = {
            @ApiResponse(
                    description = "Note not found",
                    responseCode = "404",
                    content = @Content(mediaType = "application/json"),
                    useReturnTypeSchema = true
            ),
            @ApiResponse(
                    description = "Group not found",
                    responseCode = "404",
                    content = @Content(mediaType = "application/json"),
                    useReturnTypeSchema = true
            ),
            @ApiResponse(
                    description = "Successful request",
                    responseCode = "200",
                    content = @Content(mediaType = "application/json"),
                    useReturnTypeSchema = true
            ),
            @ApiResponse(
                    description = "Validation error",
                    content = @Content(mediaType = "application/json"),
                    responseCode = "422"
            )
    })
    @PutMapping("/{id}")
    public NoteGetDTO updateNote(@PathVariable UUID id, @Valid @RequestBody NotePutDTO notePutDTO) {
        Note current = noteService.getById(id);
        Note note = noteMapper.toEntity(notePutDTO);

        if (notePutDTO.getGroupId() != null) {
            note.setGroup(groupService.getById(notePutDTO.getGroupId()));
        }

        return noteMapper.toDTO(
                noteService.updateNote(current, note)
        );
    }
    @Operation(summary = "EP for assign tag to note", responses = {
            @ApiResponse(
                    description = "Note not found",
                    responseCode = "404",
                    content = @Content(mediaType = "application/json"),
                    useReturnTypeSchema = true
            ),
            @ApiResponse(
                    description = "Tag not found",
                    responseCode = "404",
                    content = @Content(mediaType = "application/json"),
                    useReturnTypeSchema = true
            ),
            @ApiResponse(
                    description = "Successful request",
                    responseCode = "200",
                    content = @Content(mediaType = "application/json"),
                    useReturnTypeSchema = true
            )
    })
    @PatchMapping("/{noteId}/tags/{tagId}")
    public NoteGetDTO assignTagToNote(@PathVariable UUID noteId, @PathVariable UUID tagId) {
        return noteMapper.toDTO(
                noteService.assignTag(noteId, tagId)
        );
    }
    @Operation(summary = "EP for remove tag from note", responses = {
            @ApiResponse(
                    description = "Note not found",
                    responseCode = "404",
                    content = @Content(mediaType = "application/json"),
                    useReturnTypeSchema = true
            ),
            @ApiResponse(
                    description = "Tag not found",
                    responseCode = "404",
                    content = @Content(mediaType = "application/json"),
                    useReturnTypeSchema = true
            ),
            @ApiResponse(
                    description = "Successful request",
                    responseCode = "200",
                    content = @Content(mediaType = "application/json"),
                    useReturnTypeSchema = true
            )
    })
    @DeleteMapping("/{noteId}/tags/{tagId}")
    public NoteGetDTO removeTagFromNote(@PathVariable UUID noteId, @PathVariable UUID tagId) {
        return noteMapper.toDTO(
                noteService.removeTag(noteId, tagId)
        );
    }

    @Operation(summary = "EP for delete note by id")
    @DeleteMapping("/{id}")
    public void deleteNote(@PathVariable UUID id) {
        Note note = noteService.getById(id);
        noteService.deleteNote(note);
    }
}