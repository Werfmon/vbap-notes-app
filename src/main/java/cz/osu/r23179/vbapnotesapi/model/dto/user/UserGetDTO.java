package cz.osu.r23179.vbapnotesapi.model.dto.user;

import cz.osu.r23179.vbapnotesapi.model.dto.group.GroupGetDTO;
import cz.osu.r23179.vbapnotesapi.model.dto.note.NoteGetDTO;
import cz.osu.r23179.vbapnotesapi.model.entity.Group;
import cz.osu.r23179.vbapnotesapi.model.entity.Note;
import cz.osu.r23179.vbapnotesapi.model.entity.Tag;
import cz.osu.r23179.vbapnotesapi.model.entity.enums.Role;
import jakarta.persistence.Column;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.OneToMany;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class UserGetDTO {
    private UUID id;

    private String email;

    private String firstname;

    private String lastname;

    private Role role;

    private List<GroupGetDTO> groups;

    private List<NoteGetDTO> notes;

    private List<Tag> tags;
}
