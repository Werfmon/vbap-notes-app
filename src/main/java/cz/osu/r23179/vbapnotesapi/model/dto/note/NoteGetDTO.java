package cz.osu.r23179.vbapnotesapi.model.dto.note;

import cz.osu.r23179.vbapnotesapi.model.dto.group.GroupGetDTO;
import cz.osu.r23179.vbapnotesapi.model.dto.tag.TagGetDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class NoteGetDTO {
    private UUID id;
    private String title;
    private String description;
    private GroupGetDTO group;
    private List<TagGetDTO> tags;
}
