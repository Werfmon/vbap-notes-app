package cz.osu.r23179.vbapnotesapi.mapper;

import cz.osu.r23179.vbapnotesapi.model.dto.note.NoteGetDTO;
import cz.osu.r23179.vbapnotesapi.model.dto.note.NotePostDTO;
import cz.osu.r23179.vbapnotesapi.model.dto.note.NotePutDTO;
import cz.osu.r23179.vbapnotesapi.model.entity.Note;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class NoteMapper {
    private final ModelMapper modelMapper;

    public NoteGetDTO toDTO(Note note) {
        return modelMapper.map(note, NoteGetDTO.class);
    }

    public Note toEntity(NotePostDTO notePostDTO) {
        return modelMapper.map(notePostDTO, Note.class);
    }

    public Note toEntity(NotePutDTO notePutDTO) {
        return modelMapper.map(notePutDTO, Note.class);
    }

    public List<NoteGetDTO> toDTO(List<Note> notes) {
        return notes.stream()
                .map(this::toDTO)
                .toList();
    }
}
