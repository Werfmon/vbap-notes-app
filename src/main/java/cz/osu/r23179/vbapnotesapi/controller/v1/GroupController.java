package cz.osu.r23179.vbapnotesapi.controller.v1;

import cz.osu.r23179.vbapnotesapi.mapper.GroupMapper;
import cz.osu.r23179.vbapnotesapi.model.dto.group.GroupGetDTO;
import cz.osu.r23179.vbapnotesapi.model.dto.group.GroupPostDTO;
import cz.osu.r23179.vbapnotesapi.model.dto.group.GroupPutDTO;
import cz.osu.r23179.vbapnotesapi.model.entity.Group;
import cz.osu.r23179.vbapnotesapi.service.GroupService;
import cz.osu.r23179.vbapnotesapi.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/v1/groups")
@RequiredArgsConstructor
@Tag(name = "Group controller", description = "Controller for operations with groups")
public class GroupController {
    private final GroupService groupService;
    private final GroupMapper groupMapper;
    private final UserService userService;

    @Operation(summary = "EP for create group", responses = {
            @ApiResponse(
                    description = "Validation error",
                    content = @Content(mediaType = "application/json"),
                    responseCode = "422"
            ),
            @ApiResponse(
                    description = "Successful request",
                    responseCode = "200",
                    content = @Content(mediaType = "application/json"),
                    useReturnTypeSchema = true
            )
    })
    @PostMapping
    public GroupGetDTO createGroup(@Valid @RequestBody GroupPostDTO groupPostDTO) {
        Group group = groupMapper.toEntity(groupPostDTO);
        group.setUser(userService.getCurrentUser());

        return groupMapper.toDTO(
                groupService.createGroup(group)
        );
    }

    @Operation(summary = "EP for obtain group by id", responses = {
            @ApiResponse(
                    description = "Group not found",
                    responseCode = "404",
                    content = @Content(mediaType = "application/json"),
                    useReturnTypeSchema = true
            ),
            @ApiResponse(
                    description = "Successful request",
                    responseCode = "200",
                    content = @Content(mediaType = "application/json"),
                    useReturnTypeSchema = true
            )
    })
    @GetMapping("/{id}")
    public GroupGetDTO getGroupById(@PathVariable UUID id) {
        return groupMapper.toDTO(groupService.getById(id));
    }

    @Operation(summary = "EP for obtain all groups")
    @GetMapping
    public List<GroupGetDTO> getGroups() {
        return groupMapper.toDTO(groupService.getAllGroups());
    }

    @Operation(summary = "EP for update group by id", responses = {
            @ApiResponse(
                    description = "Group not found",
                    responseCode = "404",
                    content = @Content(mediaType = "application/json"),
                    useReturnTypeSchema = true
            ),
            @ApiResponse(
                    description = "Successful request",
                    responseCode = "200",
                    content = @Content(mediaType = "application/json"),
                    useReturnTypeSchema = true
            ),
            @ApiResponse(
                    description = "Validation error",
                    content = @Content(mediaType = "application/json"),
                    responseCode = "422"
            )
    })
    @PutMapping("/{id}")
    public GroupGetDTO updateGroup(@PathVariable UUID id, @Valid @RequestBody GroupPutDTO groupPutDTO) {
        Group current = groupService.getById(id);

        return groupMapper.toDTO(
                groupService.updateGroup(
                        current,
                        groupMapper.toEntity(groupPutDTO)
                )
        );
    }
    @Operation(summary = "EP for delete group by id")
    @DeleteMapping("/{id}")
    public void deleteGroup(@PathVariable UUID id) {
        Group group = groupService.getById(id);
        groupService.deleteGroup(group);
    }
}