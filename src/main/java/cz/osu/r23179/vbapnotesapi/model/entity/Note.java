package cz.osu.r23179.vbapnotesapi.model.entity;

import cz.osu.r23179.vbapnotesapi.exception.ResourceAlreadyExistsException;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Note {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID id;

    @Column(nullable = false)
    private String title;

    @Column(columnDefinition = "text", nullable = false)
    private String description;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    @ManyToMany(mappedBy = "notes", fetch = FetchType.LAZY)
    private List<Tag> tags = new ArrayList<>();

    @ManyToOne
    @JoinColumn(name = "group_id")
    private Group group;

    public Note(String title, String description, User user, List<Tag> tags, Group group) {
        this.title = title;
        this.description = description;
        this.user = user;
        this.tags = tags;
        this.group = group;
    }

    public void addTag(Tag tag) {
        boolean match = tags.stream().anyMatch(t -> t.getId() == tag.getId());
        if (match) {
            throw new ResourceAlreadyExistsException("Tag is already assigned to note");
        }
        this.tags.add(tag);
    }
    public void removeTag(Tag tag) {
        this.tags.remove(tag);
    }
}
