package cz.osu.r23179.vbapnotesapi.model.dto.group;

import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GroupPutDTO {
    @Length(max = 50, message = "Max size is 50")
    @NotBlank(message = "Name is mandatory")
    private String name;
}
