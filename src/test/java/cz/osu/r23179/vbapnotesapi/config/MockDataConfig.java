package cz.osu.r23179.vbapnotesapi.config;

import cz.osu.r23179.vbapnotesapi.model.entity.Group;
import cz.osu.r23179.vbapnotesapi.model.entity.Note;
import cz.osu.r23179.vbapnotesapi.model.entity.Tag;
import cz.osu.r23179.vbapnotesapi.model.entity.User;
import cz.osu.r23179.vbapnotesapi.model.entity.enums.Role;
import cz.osu.r23179.vbapnotesapi.repository.GroupRepository;
import cz.osu.r23179.vbapnotesapi.repository.NoteRepository;
import cz.osu.r23179.vbapnotesapi.repository.TagRepository;
import cz.osu.r23179.vbapnotesapi.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

@Configuration
@RequiredArgsConstructor
public class MockDataConfig implements CommandLineRunner {

    private final UserRepository userRepository;
    private final TagRepository tagRepository;
    private final NoteRepository noteRepository;
    private final GroupRepository groupRepository;

    @Override
    public void run(String... args) {
        List<User> users = List.of(
                new User("dominik@vyroubal.com", "Dominik", "Vyroubal", "1234", Role.ADMIN),
                new User("pepa@user.com", "Pepa", "User", "123456", Role.USER)
        );
        userRepository.saveAll(users);

        List<Tag> tags = List.of(
                new Tag("Tag 1", users.get(0)),
                new Tag("Tag 2", users.get(0))
        );
        tagRepository.saveAll(tags);

        List<Group> groups = List.of(
                new Group("Group 1", users.get(0)),
                new Group("Group 2", users.get(0)),
                new Group("Group 3", users.get(0))
        );
        groupRepository.saveAll(groups);

        List<Note> notes = List.of(
                new Note("Title of note", "Description of note", users.get(0), new ArrayList<>(), groups.get(0)),
                new Note("Title of note", "Description of note", users.get(0), new ArrayList<>(), groups.get(0)),
                new Note( "Title of note", "Description of note", users.get(1), new ArrayList<>(), groups.get(1)),
                new Note("Title of note", "Description of note", users.get(1), new ArrayList<>(), groups.get(1))
        );
        noteRepository.saveAll(notes);
    }
}
