package cz.osu.r23179.vbapnotesapi.model.entity.enums;

public enum Role {
    ADMIN,
    USER
}
