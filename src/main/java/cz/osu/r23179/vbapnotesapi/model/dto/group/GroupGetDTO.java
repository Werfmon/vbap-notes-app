package cz.osu.r23179.vbapnotesapi.model.dto.group;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GroupGetDTO {
    private UUID id;
    private String name;
}
