package cz.osu.r23179.vbapnotesapi.model.dto.user;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class UserPostDTO {
    @Email(message = "Email has invalid format")
    @NotBlank(message = "Email is mandatory")
    private String email;

    @NotBlank(message = "FirstName is mandatory")
    private String firstName;

    @NotBlank(message = "LastName is mandatory")
    private String lastName;

    @NotBlank(message = "Password is mandatory")
    @Length(min = 8, message = "Minimum length is 8 characters")
    private String password;
}
