package cz.osu.r23179.vbapnotesapi.service.impl;

import cz.osu.r23179.vbapnotesapi.exception.ResourceAlreadyExistsException;
import cz.osu.r23179.vbapnotesapi.exception.ResourceNotFoundException;
import cz.osu.r23179.vbapnotesapi.mapper.UserMapper;
import cz.osu.r23179.vbapnotesapi.model.entity.User;
import cz.osu.r23179.vbapnotesapi.repository.UserRepository;
import cz.osu.r23179.vbapnotesapi.service.UserService;
import cz.osu.r23179.vbapnotesapi.util.JwtTokenUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    private final UserMapper userMapper;

    private final AuthenticationManager authenticationManager;

    private final JwtTokenUtil jwtTokenUtil;

    private final UserDetailsService userDetailsService;

    @Override
    public User createUser(User user) {
        if (userRepository.existsByEmail(user.getEmail())) {
            throw new ResourceAlreadyExistsException(STR."User with email: \{user.getEmail()} exists");
        }
        return userRepository.save(user);
    }

    @Override
    public User getById(UUID id) {
        return userRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(STR."Cannot find user with id: \{id}"));
    }

    @Override
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public User updateUser(User current, User user) {
        current.setFirstName(user.getFirstName());
        current.setLastName(user.getLastName());
        return userRepository.save(current);
    }

    @Override
    public User getCurrentUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String email = authentication.getName();
        return getUserByEmail(email);
    }

    @Override
    public User getUserByEmail(String email) {
        return userRepository.findByEmail(email).orElseThrow(
                () -> new ResourceNotFoundException(STR."User not found with email: \{email}")
        );
    }

    @Override
    public String authenticateUser(String email, String password) {
        log.info("User with email '{}' is trying to log in.", email);
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(email, password));

        log.info("User with email '{}' successfully authenticated.", email);
        return jwtTokenUtil.generateAccessToken(userDetailsService.loadUserByUsername(email));

    }

    @Override
    public void deleteUser(User user) {
        userRepository.delete(user);
    }
}
