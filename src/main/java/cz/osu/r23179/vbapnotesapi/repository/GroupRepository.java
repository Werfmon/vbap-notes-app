package cz.osu.r23179.vbapnotesapi.repository;

import cz.osu.r23179.vbapnotesapi.model.entity.Group;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface GroupRepository extends JpaRepository<Group, UUID> {
}
