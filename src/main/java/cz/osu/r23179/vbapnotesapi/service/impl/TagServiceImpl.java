package cz.osu.r23179.vbapnotesapi.service.impl;

import cz.osu.r23179.vbapnotesapi.exception.ResourceNotFoundException;
import cz.osu.r23179.vbapnotesapi.model.entity.Tag;
import cz.osu.r23179.vbapnotesapi.repository.TagRepository;
import cz.osu.r23179.vbapnotesapi.service.TagService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class TagServiceImpl implements TagService {

    private final TagRepository tagRepository;

    public Tag saveTag(Tag tag) {
        return tagRepository.save(tag);
    }

    @Override
    public List<Tag> saveAllTags(List<Tag> tags) {
        return tagRepository.saveAll(tags);
    }

    @Override
    public Tag getById(UUID id) {
        return tagRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(STR."Cannot find tag with id: \{id}"));
    }

    @Override
    public List<Tag> getAllTags() {
        return tagRepository.findAll();
    }

    @Override
    public Tag updateTag(Tag current, Tag tag) {
        current.setName(tag.getName());
        return tagRepository.save(current);
    }

    @Override
    public void deleteTag(Tag tag) {
        tagRepository.delete(tag);
    }
}
