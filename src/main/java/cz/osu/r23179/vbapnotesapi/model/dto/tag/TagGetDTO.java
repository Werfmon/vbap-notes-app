package cz.osu.r23179.vbapnotesapi.model.dto.tag;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TagGetDTO {
    private UUID id;
    private String name;
}
