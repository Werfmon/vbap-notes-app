package cz.osu.r23179.vbapnotesapi.controller.v1;

import cz.osu.r23179.vbapnotesapi.mapper.UserMapper;
import cz.osu.r23179.vbapnotesapi.model.AuthenticationRequest;
import cz.osu.r23179.vbapnotesapi.model.AuthenticationResponse;
import cz.osu.r23179.vbapnotesapi.model.dto.user.UserGetDTO;
import cz.osu.r23179.vbapnotesapi.model.dto.user.UserPostDTO;
import cz.osu.r23179.vbapnotesapi.model.dto.user.UserPutDTO;
import cz.osu.r23179.vbapnotesapi.model.entity.User;
import cz.osu.r23179.vbapnotesapi.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/v1/users")
@RequiredArgsConstructor
@Tag(name = "User controller", description = "Controller for operations with users")
public class UserController {
    private final UserService userService;
    private final UserMapper userMapper;

    @Operation(summary = "Authenticate user and obtain access token", responses = {
            @ApiResponse(
                    description = "Authentication successful",
                    responseCode = "200",
                    content = @Content(mediaType = "application/json")
            ),
            @ApiResponse(
                    description = "Unauthorized",
                    responseCode = "401",
                    content = @Content(mediaType = "application/json")
            ),
            @ApiResponse(
                    description = "User not found",
                    responseCode = "404",
                    content = @Content(mediaType = "application/json")
            ),
            @ApiResponse(
                    description = "Validation error",
                    content = @Content(mediaType = "application/json"),
                    responseCode = "422"
            )
    })
    @PostMapping("/authenticate")
    @ResponseStatus(HttpStatus.OK)
    public AuthenticationResponse authenticateUser(@Valid @RequestBody AuthenticationRequest authenticationRequest) {

        String token = userService.authenticateUser(
                authenticationRequest.getEmail(),
                authenticationRequest.getPassword()
        );

        return new AuthenticationResponse(token);
    }
    @Operation(summary = "EP for create user", responses = {
            @ApiResponse(
                    description = "Validation error",
                    content = @Content(mediaType = "application/json"),
                    responseCode = "422"
            ),
            @ApiResponse(
                    description = "Successful request",
                    responseCode = "200",
                    content = @Content(mediaType = "application/json"),
                    useReturnTypeSchema = true
            )
    })
    @PostMapping
    public UserGetDTO createUser(@Valid @RequestBody UserPostDTO userPostDTO) {
        return userMapper.toDTO(
                userService.createUser(
                        userMapper.toEntity(userPostDTO)
                )
        );
    }
    @Operation(summary = "EP for obtain user by id", responses = {
            @ApiResponse(
                    description = "Group not found",
                    responseCode = "404",
                    content = @Content(mediaType = "application/json"),
                    useReturnTypeSchema = true
            ),
            @ApiResponse(
                    description = "Successful request",
                    responseCode = "200",
                    content = @Content(mediaType = "application/json"),
                    useReturnTypeSchema = true
            )
    })
    @GetMapping("/{id}")
    public UserGetDTO getUserById(@PathVariable UUID id) {
        return userMapper.toDTO(userService.getById(id));
    }

    @Operation(summary = "EP for obtain all users")
    @GetMapping
    public List<UserGetDTO> getUsers() {
        return userMapper.toDTO(userService.getAllUsers());
    }


    @Operation(summary = "EP for update user by id", responses = {
            @ApiResponse(
                    description = "User not found",
                    responseCode = "404",
                    content = @Content(mediaType = "application/json"),
                    useReturnTypeSchema = true
            ),
            @ApiResponse(
                    description = "Successful request",
                    responseCode = "200",
                    content = @Content(mediaType = "application/json"),
                    useReturnTypeSchema = true
            ),
            @ApiResponse(
                    description = "Validation error",
                    content = @Content(mediaType = "application/json"),
                    responseCode = "422"
            )
    })
    @PutMapping("/{id}")
    public UserGetDTO updateUser(@PathVariable UUID id, @Valid @RequestBody UserPutDTO userPutDTO) {
        User current = userService.getById(id);

        return userMapper.toDTO(
                userService.updateUser(
                        current,
                        userMapper.toEntity(userPutDTO)
                )
        );
    }

    @Operation(summary = "EP for delete user by id")
    @DeleteMapping("/{id}")
    public void deleteUser(@PathVariable UUID id) {
        User user = userService.getById(id);
        userService.deleteUser(user);
    }
}