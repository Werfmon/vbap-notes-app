package cz.osu.r23179.vbapnotesapi.mapper;

import cz.osu.r23179.vbapnotesapi.model.dto.group.GroupGetDTO;
import cz.osu.r23179.vbapnotesapi.model.dto.group.GroupPostDTO;
import cz.osu.r23179.vbapnotesapi.model.dto.group.GroupPutDTO;
import cz.osu.r23179.vbapnotesapi.model.dto.user.UserGetDTO;
import cz.osu.r23179.vbapnotesapi.model.dto.user.UserPostDTO;
import cz.osu.r23179.vbapnotesapi.model.dto.user.UserPutDTO;
import cz.osu.r23179.vbapnotesapi.model.entity.Group;
import cz.osu.r23179.vbapnotesapi.model.entity.User;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class GroupMapper {
    private final ModelMapper modelMapper;

    public GroupGetDTO toDTO(Group group) {
        return modelMapper.map(group, GroupGetDTO.class);
    }

    public Group toEntity(GroupPostDTO groupPostDTO) {
        return modelMapper.map(groupPostDTO, Group.class);
    }

    public Group toEntity(GroupPutDTO groupPutDTO) {
        return modelMapper.map(groupPutDTO, Group.class);
    }

    public List<GroupGetDTO> toDTO(List<Group> groups) {
        return groups.stream()
                .map(this::toDTO)
                .toList();
    }
}
