FROM maven:3-amazoncorretto-21-debian

WORKDIR /usr/src/app

COPY . /usr/src/app

ARG PROFILE=development
ARG PORT=8080
ARG JAVA_HOME

ENV SPRING_PROFILES_ACTIVE=$PROFILE
ENV SERVER_PORT=$PORT

RUN echo "---------------------- "
RUN echo "ACTIVE PROFILE=$SPRING_PROFILES_ACTIVE"
RUN echo "PORT=$PORT"
RUN echo "---------------------- "

RUN mvn clean
RUN mvn package -DskipTests

ENTRYPOINT [ "sh", "-c", "mvn spring-boot:run" ]

