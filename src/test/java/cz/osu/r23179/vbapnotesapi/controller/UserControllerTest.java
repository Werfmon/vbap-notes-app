package cz.osu.r23179.vbapnotesapi.controller;

import cz.osu.r23179.vbapnotesapi.model.dto.user.UserGetDTO;
import cz.osu.r23179.vbapnotesapi.model.entity.enums.Role;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.test.annotation.DirtiesContext;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DirtiesContext(classMode =  DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class UserControllerTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    private HttpHeaders getHeaders() {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        httpHeaders.setAccept(List.of(MediaType.APPLICATION_JSON));
        return httpHeaders;
    }

    @Test
    void itShouldCreateUser() throws JSONException {
        JSONObject newUser = new JSONObject();
        newUser.put("email", "dominik@vyroubal.cz");
        newUser.put("firstName", "Dominik");
        newUser.put("lastName", "Vyroubal");
        newUser.put("password", "12345678");

        HttpEntity<String> request = new HttpEntity<>(newUser.toString(), getHeaders());

        ResponseEntity<UserGetDTO> response = testRestTemplate.postForEntity("/v1/users", request, UserGetDTO.class);

        assertAll(
                () -> assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK),
                () -> assertThat(response.getBody().getEmail()).isEqualTo("dominik@vyroubal.cz"),
                () -> assertThat(response.getBody().getRole()).isEqualTo(Role.USER)
        );
    }

    @Test
    void itShouldReturn409Conflict() throws JSONException {
        JSONObject newUser = new JSONObject();
        newUser.put("email", "dominik@vyroubal.com");
        newUser.put("firstName", "Dominik");
        newUser.put("lastName", "Vyroubal");
        newUser.put("password", "12345678");

        HttpEntity<String> request = new HttpEntity<>(newUser.toString(), getHeaders());

        ResponseEntity<UserGetDTO> response = testRestTemplate.postForEntity("/v1/users", request, UserGetDTO.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CONFLICT);
    }
}
