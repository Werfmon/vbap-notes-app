package cz.osu.r23179.vbapnotesapi.repository;

import cz.osu.r23179.vbapnotesapi.model.entity.Note;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface NoteRepository extends JpaRepository<Note, UUID> {
}
