package cz.osu.r23179.vbapnotesapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VbapNotesApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(VbapNotesApiApplication.class, args);
    }

}
