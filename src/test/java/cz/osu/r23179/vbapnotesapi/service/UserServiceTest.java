package cz.osu.r23179.vbapnotesapi.service;

import cz.osu.r23179.vbapnotesapi.exception.ResourceNotFoundException;
import cz.osu.r23179.vbapnotesapi.model.entity.User;
import cz.osu.r23179.vbapnotesapi.repository.UserRepository;
import cz.osu.r23179.vbapnotesapi.service.impl.UserServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {

    @InjectMocks
    private UserServiceImpl userService;

    @Mock
    private UserRepository userRepository;

    @Test
    public void givenCurrentUserAndNewUser_whenMapNewUserToOldOne_thenSaveViaRepository() {
        // given
        User current = new User();
        current.setFirstName("Dominik");
        current.setLastName("Vyroubal");

        User user = new User();
        user.setFirstName("Matus");
        user.setLastName("Novak");

        // when
        when(userRepository.save(current)).thenReturn(current);
        User saved = userService.updateUser(current, user);

        // then
        assertEquals(user.getFirstName(), saved.getFirstName(), "First name is not same as expected");
        assertEquals(user.getLastName(), saved.getLastName(), "Last name is not same as expected");
        verify(userRepository, times(1)).save(current);
    }

    @Test
    public void givenUserId_whenLoadFromDatabase_thenReturnUser() {
        // given
        UUID userId = UUID.randomUUID();
        User user = new User();
        user.setId(userId);
        Optional<User> optionalUser = Optional.of(user);

        // when
        when(userRepository.findById(userId)).thenReturn(optionalUser);
        User returned = userService.getById(userId);

        // then
        assertEquals(userId, returned.getId(), "Id is not same as expected");
        verify(userRepository, times(1)).findById(userId);
    }

    @Test
    public void givenUserId_whenLoadFromDatabase_thenThrowResourceNotFoundException() {
        // given
        UUID userId = UUID.randomUUID();

        // when
        when(userRepository.findById(userId)).thenReturn(Optional.empty());

        // then
        assertThrows(ResourceNotFoundException.class, () -> userService.getById(userId), "It should throw ResourceNotFoundException");
    }

    @Test
    public void givenUserEmail_whenLoadFromDatabase_thenReturnUser() {
        // given
        String email = "dominik@vyroubal.com";

        User user = new User();
        user.setEmail(email);

        Optional<User> optionalUser = Optional.of(user);

        // when
        when(userRepository.findByEmail(email)).thenReturn(optionalUser);
        User returned = userService.getUserByEmail(email);

        // then
        assertNotNull(returned, "Returned User entity is null, cannot be null");
        assertEquals(email, returned.getEmail(), "Email is not same as expected");
        verify(userRepository, times(1)).findByEmail(email);
    }

    @Test
    public void givenListOfUsers_whenLoadFromDatabase_thenReturnListOfUser() {
        // given
        List<User> users = List.of(new User());

        // when
        when(userRepository.findAll()).thenReturn(users);
        List<User> returned = userService.getAllUsers();

        // then
        assertEquals(users.size(), returned.size(), "Size of list of Users is not same as expected");
        verify(userRepository, times(1)).findAll();
    }
}
