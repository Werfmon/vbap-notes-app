package cz.osu.r23179.vbapnotesapi.service;

import cz.osu.r23179.vbapnotesapi.model.entity.Note;

import java.util.List;
import java.util.UUID;

public interface NoteService {
    Note saveNote(Note note);

    List<Note> saveAllNotes(List<Note> notes);

    Note getById(UUID id);

    List<Note> getAllNotes();

    Note updateNote(Note current, Note note);

    Note assignTag(UUID noteId, UUID tagId);
    Note removeTag(UUID noteId, UUID tagId);

    void deleteNote(Note note);
}
