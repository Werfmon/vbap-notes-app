package cz.osu.r23179.vbapnotesapi.service.impl;

import cz.osu.r23179.vbapnotesapi.exception.ResourceNotFoundException;
import cz.osu.r23179.vbapnotesapi.model.entity.Group;
import cz.osu.r23179.vbapnotesapi.model.entity.Note;
import cz.osu.r23179.vbapnotesapi.model.entity.Tag;
import cz.osu.r23179.vbapnotesapi.repository.GroupRepository;
import cz.osu.r23179.vbapnotesapi.repository.NoteRepository;
import cz.osu.r23179.vbapnotesapi.service.GroupService;
import cz.osu.r23179.vbapnotesapi.service.NoteService;
import cz.osu.r23179.vbapnotesapi.service.TagService;
import cz.osu.r23179.vbapnotesapi.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class NoteServiceImpl implements NoteService {

    private final NoteRepository noteRepository;
    private final TagService tagService;

    @Override
    public Note saveNote(Note note) {
        return noteRepository.save(note);
    }

    @Override
    public List<Note> saveAllNotes(List<Note> notes) {
        return noteRepository.saveAll(notes);
    }

    @Override
    public Note getById(UUID id) {
        return noteRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(STR."Cannot find note with id: \{id}"));
    }

    @Override
    public List<Note> getAllNotes() {
        return noteRepository.findAll();
    }

    @Override
    public Note updateNote(Note current, Note note) {
        current.setTitle(note.getTitle());
        current.setTitle(note.getDescription());
        current.setGroup(note.getGroup());
        return noteRepository.save(current);
    }

    @Override
    public Note assignTag(UUID noteId, UUID tagId) {
        Note note = this.getById(noteId);
        Tag tag = tagService.getById(tagId);

        note.addTag(tag);
        tag.addNote(note);

        tagService.saveTag(tag);
        return noteRepository.save(note);
    }

    @Override
    public Note removeTag(UUID noteId, UUID tagId) {
        Note note = this.getById(noteId);
        Tag tag = tagService.getById(tagId);

        note.removeTag(tag);
        tag.removeNote(note);

        tagService.saveTag(tag);
        return noteRepository.save(note);

    }

    @Override
    public void deleteNote(Note note) {
        List<Tag> tags = note.getTags().stream().peek(tag -> tag.removeNote(note)).toList();
        tagService.saveAllTags(tags);
        noteRepository.delete(note);
    }
}
