package cz.osu.r23179.vbapnotesapi.mapper;

import cz.osu.r23179.vbapnotesapi.model.dto.tag.TagGetDTO;
import cz.osu.r23179.vbapnotesapi.model.dto.tag.TagPostDTO;
import cz.osu.r23179.vbapnotesapi.model.dto.tag.TagPutDTO;
import cz.osu.r23179.vbapnotesapi.model.entity.Tag;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class TagMapper {
    private final ModelMapper modelMapper;

    public TagGetDTO toDTO(Tag tag) {
        return modelMapper.map(tag, TagGetDTO.class);
    }

    public Tag toEntity(TagPostDTO tagPostDTO) {
        return modelMapper.map(tagPostDTO, Tag.class);
    }

    public Tag toEntity(TagPutDTO tagPutDTO) {
        return modelMapper.map(tagPutDTO, Tag.class);
    }

    public List<TagGetDTO> toDTO(List<Tag> tags) {
        return tags.stream()
                .map(this::toDTO)
                .toList();
    }
}
