package cz.osu.r23179.vbapnotesapi.service;

import cz.osu.r23179.vbapnotesapi.model.entity.Group;

import java.util.List;
import java.util.UUID;

public interface GroupService {
    Group createGroup(Group group);

    Group getById(UUID id);

    List<Group> getAllGroups();

    Group updateGroup(Group current, Group group);

    void deleteGroup(Group group);
}
