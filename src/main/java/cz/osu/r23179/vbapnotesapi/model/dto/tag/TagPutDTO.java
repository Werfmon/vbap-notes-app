package cz.osu.r23179.vbapnotesapi.model.dto.tag;

import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TagPutDTO {
    @Length(max = 30, message = "Name has max length 30 characters")
    @NotBlank(message = "Name is mandatory")
    private String name;
}
