package cz.osu.r23179.vbapnotesapi.mapper;

import cz.osu.r23179.vbapnotesapi.model.dto.user.UserGetDTO;
import cz.osu.r23179.vbapnotesapi.model.dto.user.UserPostDTO;
import cz.osu.r23179.vbapnotesapi.model.dto.user.UserPutDTO;
import cz.osu.r23179.vbapnotesapi.model.entity.User;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class UserMapper {
    private final ModelMapper modelMapper;

    public UserGetDTO toDTO(User user) {
        return modelMapper.map(user, UserGetDTO.class);
    }

    public User toEntity(UserPostDTO userPostDTO) {
        return modelMapper.map(userPostDTO, User.class);
    }

    public User toEntity(UserPutDTO userPutDTO) {
        return modelMapper.map(userPutDTO, User.class);
    }

    public List<UserGetDTO> toDTO(List<User> users) {
        return users.stream()
                .map(this::toDTO)
                .toList();
    }
}
