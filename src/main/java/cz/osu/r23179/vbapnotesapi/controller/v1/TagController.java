package cz.osu.r23179.vbapnotesapi.controller.v1;

import cz.osu.r23179.vbapnotesapi.mapper.TagMapper;
import cz.osu.r23179.vbapnotesapi.model.dto.tag.TagGetDTO;
import cz.osu.r23179.vbapnotesapi.model.dto.tag.TagPostDTO;
import cz.osu.r23179.vbapnotesapi.model.dto.tag.TagPutDTO;
import cz.osu.r23179.vbapnotesapi.model.entity.Tag;
import cz.osu.r23179.vbapnotesapi.service.TagService;
import cz.osu.r23179.vbapnotesapi.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/v1/tags")
@RequiredArgsConstructor
@io.swagger.v3.oas.annotations.tags.Tag(name = "Tag controller", description = "Controller for operations with tags")
public class TagController {
    private final TagService tagService;
    private final TagMapper tagMapper;
    private final UserService userService;

    @Operation(summary = "EP for create tag", responses = {
            @ApiResponse(
                    description = "Validation error",
                    content = @Content(mediaType = "application/json"),
                    responseCode = "422"
            ),
            @ApiResponse(
                    description = "Successful request",
                    responseCode = "200",
                    content = @Content(mediaType = "application/json"),
                    useReturnTypeSchema = true
            )
    })
    @PostMapping
    public TagGetDTO createTag(@Valid @RequestBody TagPostDTO tagPostDTO) {
        Tag tag = tagMapper.toEntity(tagPostDTO);
        tag.setUser(userService.getCurrentUser());

        return tagMapper.toDTO(
                tagService.saveTag(tag)
        );
    }

    @Operation(summary = "EP for obtain tag by id", responses = {
            @ApiResponse(
                    description = "Tag not found",
                    responseCode = "404",
                    content = @Content(mediaType = "application/json"),
                    useReturnTypeSchema = true
            ),
            @ApiResponse(
                    description = "Successful request",
                    responseCode = "200",
                    content = @Content(mediaType = "application/json"),
                    useReturnTypeSchema = true
            )
    })
    @GetMapping("/{id}")
    public TagGetDTO getTagById(@PathVariable UUID id) {
        return tagMapper.toDTO(tagService.getById(id));
    }

    @Operation(summary = "EP for obtain all tags")
    @GetMapping
    public List<TagGetDTO> getTags() {
        return tagMapper.toDTO(tagService.getAllTags());
    }

    @Operation(summary = "EP for update tag by id", responses = {
            @ApiResponse(
                    description = "Tag not found",
                    responseCode = "404",
                    content = @Content(mediaType = "application/json"),
                    useReturnTypeSchema = true
            ),
            @ApiResponse(
                    description = "Successful request",
                    responseCode = "200",
                    content = @Content(mediaType = "application/json"),
                    useReturnTypeSchema = true
            ),
            @ApiResponse(
                    description = "Validation error",
                    content = @Content(mediaType = "application/json"),
                    responseCode = "422"
            )
    })
    @PutMapping("/{id}")
    public TagGetDTO updateTag(@PathVariable UUID id, @Valid @RequestBody TagPutDTO tagPutDTO) {
        Tag current = tagService.getById(id);

        return tagMapper.toDTO(
                tagService.updateTag(
                        current,
                        tagMapper.toEntity(tagPutDTO)
                )
        );
    }

    @Operation(summary = "EP for delete tag by id")
    @DeleteMapping("/{id}")
    public void deleteTag(@PathVariable UUID id) {
        Tag tag = tagService.getById(id);
        tagService.deleteTag(tag);
    }
}