package cz.osu.r23179.vbapnotesapi.service.impl;

import cz.osu.r23179.vbapnotesapi.exception.ResourceNotFoundException;
import cz.osu.r23179.vbapnotesapi.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService {

    private final UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String email) {
        cz.osu.r23179.vbapnotesapi.model.entity.User user = userRepository.findByEmail(email).orElseThrow(
                () -> new ResourceNotFoundException(STR."User with email '\{email}' cannot be authorized, does not exists")
        );

        return new User(
                user.getEmail(),
                user.getPassword(),
                new HashSet<>(Set.of(new SimpleGrantedAuthority("ROLE_" + user.getRole().name()))));
    }
}
