package cz.osu.r23179.vbapnotesapi.service;

import cz.osu.r23179.vbapnotesapi.model.entity.Tag;

import java.util.List;
import java.util.UUID;

public interface TagService {
    Tag saveTag(Tag tag);

    List<Tag> saveAllTags(List<Tag> tags);

    Tag getById(UUID id);

    List<Tag> getAllTags();

    Tag updateTag(Tag current, Tag group);

    void deleteTag(Tag tag);
}
